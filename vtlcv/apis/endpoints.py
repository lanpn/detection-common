import flask
import os
import uuid
import base64

from flask import current_app
from flask_restful import Resource

from vtlcv.apis.utils import err_logged, payload_required
from vtlcv.model import *


class CutModuleEndpoint(Resource):
    @err_logged
    @payload_required
    def post(self):
        payload = flask.request.get_json()
        requests = payload.get('requests', [])
        debug = payload.get('debug', False)
        
        images = list(map(
            lambda x: CardCuttingRequestSchema().load(x).data.image, requests
        ))
        img_paths = list(map(
            lambda x: x.resolve_uri(), images
        ))

        engine = current_app.config['CUT_ENGINE']
        try:
            results = engine.get_results(img_paths, debug)
        except AssertionError as e:
            return {
                'msg': ', '.join(e.args)
            }, 400
        
        return {'responses': results}


class TextBoxModuleEndpoint(Resource):
    @err_logged
    @payload_required
    def post(self):
        payload = flask.request.get_json()
        requests = payload.get('requests', [])
        debug = payload.get('debug', False)
        
        images = list(map(
            lambda x: TextBoxRequestSchema().load(x).data.image, requests
        ))
        img_paths = list(map(
            lambda x: x.resolve_uri(), images
        ))

        engine = current_app.config['TEXTBOX_ENGINE']
        try:
            results = engine.get_results(img_paths, debug)
        except AssertionError as e:
            return {
                'msg': ', '.join(e.args)
            }, 400
        
        return {'responses': results}


class DetectModuleEndpoint(Resource):
    @err_logged
    @payload_required
    def post(self):
        payload = flask.request.get_json()
        requests = payload.get('requests', [])
        debug = payload.get('debug', False)
        
        images = list(map(
            lambda x: TextDetectionRequestSchema().load(x).data.image, requests
        ))
        img_paths = list(map(
            lambda x: x.resolve_uri(), images
        ))

        engine = current_app.config['DETECT_ENGINE']
        try:
            results = engine.get_results(img_paths, debug)
        except AssertionError as e:
            return {
                'msg': ', '.join(e.args)
            }, 400
        
        return {'responses': results}


class PostProcessModuleEndpoint(Resource):
    @err_logged
    @payload_required
    def post(self):
        payload = flask.request.get_json()
        requests = payload.get('requests', [])
        debug = payload.get('debug', False)
        
        strings = list(map(
            lambda x: PostProcessRequestSchema().load(x).data.content, requests
        ))

        engine = current_app.config['POSTPROCESS_ENGINE']
        try:
            results = engine.get_results(strings, debug)
        except AssertionError as e:
            return {
                'msg': ', '.join(e.args)
            }, 400
        
        return {'responses': results}
