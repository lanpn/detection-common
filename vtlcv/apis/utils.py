import base64
import logging

from flask import request


def err_logged(fn, msg=''):
    def decorate(*args, **kwargs):
        logger = logging.getLogger('flask.error')
        try:
            return fn(*args, **kwargs)
        except Exception as e:
            logger.exception(msg, exc_info=True)
            raise e
    return decorate


def payload_required(fn):
    def decorate(*args, **kwargs):
        logger = logging.getLogger('flask.error')
        payload = request.get_json()
        if payload is None:
            return {
                'msg': 'A JSON body is required in the POST request'
            }, 400
        return fn(*args, **kwargs)
    return decorate


def decode_base64_image(b64_str):
    return base64.b64decode(b64_str)


def encode_base64_image(path):
    with open(path, 'rb') as f:
        s = f.read()
    return base64.b64encode(s)
