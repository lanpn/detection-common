import flask
import logging
import os
import traceback
import logging.config
import atexit

from flask import Flask
from flask_logconfig import LogConfig
from flask_cors import CORS
from flask_restful import Api

from vtlcv.apis.endpoints import CutModuleEndpoint, TextBoxModuleEndpoint, DetectModuleEndpoint, PostProcessModuleEndpoint


app = Flask(__name__)

# App configs
app.config.from_pyfile('/app/configs/app.default.py')
CONFIG_ENVVAR = 'APP_CONF'
if CONFIG_ENVVAR in os.environ.keys():
    app.config.from_envvar(CONFIG_ENVVAR, silent=True)
app.config.from_pyfile('/app/configs/node.py')
logger = logging.getLogger('app')

# Modifiers
CORS(app)
LogConfig(app)

# Add flask_restful endpoints
api = Api(app)

if 'CUT_ENGINE' in app.config.keys():
    api.add_resource(CutModuleEndpoint, '/v1/images:get-card')
if 'TEXTBOX_ENGINE' in app.config.keys():
    api.add_resource(TextBoxModuleEndpoint, '/v1/images:get-textbox')
if 'DETECT_ENGINE' in app.config.keys():
    api.add_resource(DetectModuleEndpoint, '/v1/images:get-text-content')
if 'POSTPROCESS_ENGINE' in app.config.keys():
    api.add_resource(PostProcessModuleEndpoint, '/v1/images:process-content')


def exit():
    pass

atexit.register(exit)

if __name__ == '__main__':
    app.run(host=app.config['HOST'],
            port=app.config['PORT'])
    print()
