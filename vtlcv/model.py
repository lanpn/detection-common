import logging
import os
import numpy as np
import cv2

from marshmallow import Schema, fields, post_load


class Image:
    IMAGE_ROOT_FOLDER = os.environ.get('CV_LOGS_DIR', '/logs')

    def __init__(self, uri):
        self.uri = uri

    def resolve_uri(self):
        path = self.uri

        if path.startswith('file://'):
            path = path[7:]
            path = os.path.join(self.IMAGE_ROOT_FOLDER, path)

        return path

    @classmethod
    def relative_uri(cls, uri):
        uri = uri.replace(cls.IMAGE_ROOT_FOLDER, '')
        if uri.startswith('/'):
            uri = uri[1:]
        return 'file://' + uri


class ImageSchema(Schema):
    uri = fields.Str()

    @post_load
    def make_obj(self, data):
        return Image(**data)


'''
Response schemas
'''
class Vertex:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def array(self):
        return [self.x, self.y]

    def denorm(self, shape):
        h, w = shape[:2]
        return Vertex(self.x * w, self.y * h)

    def norm(self, shape):
        h, w = shape[:2]
        return Vertex(self.x / w, self.y / h)

    def __str__(self):
        return 'Vertex(%.2f, %.2f)' % (self.x, self.y)

    def __repr__(self):
        return str(self)


class VertexSchema(Schema):
    x = fields.Float()
    y = fields.Float()

    @post_load
    def make_obj(self, data):
        return Vertex(**data)


class BoundingPoly:
    def __init__(self, vertices):
        self.vertices = vertices

    def denorm(self, shape):
        return BoundingPoly([
            v.denorm(shape) for v in self.vertices
        ])

    def norm(self, shape):
        return BoundingPoly([
            v.norm(shape) for v in self.vertices
        ])

    def array(self):
        return [v.array() for v in self.vertices]

    @classmethod
    def from_rect_box(cls, box):
        tl, br = box
        x1, y1 = tl
        x2, y2 = br
        tr = (x2, y1)
        bl = (x1, y2)
        return cls([
            Vertex(*tl), Vertex(*tr),
            Vertex(*br), Vertex(*bl)
        ])
    
    @classmethod
    def bounding_rect(cls, polys):
        points = []
        for poly in polys:
            for point in poly.array():
                points.append([point])
        points = np.asarray(points, dtype='int')
        x, y, w, h = cv2.boundingRect(points)
        box = [
            [x, y], [x + w, y + h]
        ]
        return cls.from_rect_box(box)

class BoundingPolySchema(Schema):
    vertices = fields.List(fields.Nested(VertexSchema()))

    @post_load
    def make_obj(self, data):
        return BoundingPoly(**data)


class CardCuttingResult:
    def __init__(self, boundingPoly: BoundingPoly, confidence: float, image=None):
        self.boundingPoly = boundingPoly
        self.confidence = confidence
        self.image = image
        # Round out
        self.confidence = float(np.round(confidence, 3))

class CardCuttingResultSchema(Schema):
    boundingPoly = fields.Nested(BoundingPolySchema())
    confidence = fields.Float()
    image = fields.Nested(ImageSchema(), missing=None)

    @post_load
    def make_obj(self, data):
        return CardCuttingResult(**data)


class TextBoxResult:
    FIELD_IDENTITY_CARD = 'identityCard'
    FIELD_IDENTITY_ID = 'identityId'
    FIELD_IDENTITY_NAME = 'identityName'
    FIELD_IDENTITY_BIRTHPLACE = 'identityBirthplace'
    FIELD_IDENTITY_BIRTHDAY = 'identityBirthday'
    FIELD_IDENTITY_ADDRESS = 'identityAddress'

    def __init__(self, boundingPoly: BoundingPoly, field: str,
        confidence: float, image=None):
        self.boundingPoly = boundingPoly
        self.confidence = confidence
        self.field = field
        self.image = image
        
        # Round out
        self.confidence = float(np.round(confidence, 3))
    @post_load
    def make_obj(self, data):
        return CardCuttingResult(**data)


class TextBoxResultSchema(Schema):
    boundingPoly = fields.Nested(BoundingPolySchema())
    image = fields.Nested(ImageSchema(), missing=None)
    field = fields.Str()
    confidence = fields.Float()

    @post_load
    def make_obj(self, data):
        return TextBoxResult(**data)


class TextDetectionResult:
    def __init__(self, description: str, confidence: float):
        self.confidence = confidence
        self.description = description
        # Round out
        self.confidence = float(np.round(confidence, 3))


class TextDetectionResultSchema(Schema):
    description = fields.Str()
    confidence = fields.Float()

    @post_load
    def make_obj(self, data):
        return TextDetectionResult(**data)


class PostProcessResult:
    def __init__(self, content: str):
        self.content = content


class PostProcessResultSchema(Schema):
    content = fields.Str()

    @post_load
    def make_obj(self, data):
        return PostProcessResult(**data)


'''
Request schemas
'''
class CardCuttingRequest:
    def __init__(self, image: Image):
        self.image = image


class CardCuttingRequestSchema(Schema):
    image = fields.Nested(ImageSchema())

    @post_load
    def make_obj(self, data):
        return CardCuttingRequest(**data)


class TextBoxRequest:
    def __init__(self, image: Image):
        self.image = image


class TextBoxRequestSchema(Schema):
    image = fields.Nested(ImageSchema())

    @post_load
    def make_obj(self, data):
        return TextBoxRequest(**data)


class TextDetectionRequest:
    def __init__(self, image: Image):
        self.image = image


class TextDetectionRequestSchema(Schema):
    image = fields.Nested(ImageSchema())

    @post_load
    def make_obj(self, data):
        return TextDetectionRequest(**data)


class PostProcessRequest:
    def __init__(self, content: str):
        self.content = content


class PostProcessRequestSchema(Schema):
    content = fields.Str()

    @post_load
    def make_obj(self, data):
        return PostProcessRequest(**data)
