import cv2
import numpy as np
import os

from typing import List
from PIL import Image as PILImage

from vtlcv.model import CardCuttingResult,\
    TextBoxResult, TextDetectionResult,\
    CardCuttingResultSchema, TextBoxResultSchema,\
    TextDetectionResultSchema, PostProcessRequest,\
    PostProcessRequestSchema, PostProcessResult,\
    PostProcessResultSchema, BoundingPoly, Image,\
    Vertex


def _cv_load_stub(path):
    arr = PILImage.open(path)
    arr = np.asarray(arr)
    arr = arr[:, :, [2, 1, 0]]
    return arr


class Engine:
    @staticmethod
    def segment_rotate_image(img, bounding_poly):
        pts = bounding_poly.array()
        pts = np.asarray(pts, dtype='float32')

        (tl, tr, br, bl) = pts

        widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
        widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
        maxWidth = max(int(widthA), int(widthB))

        heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
        heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
        maxHeight = max(int(heightA), int(heightB))

        #BOTTOM_PAD = int(maxHeight * 0.05)
        dst = np.array([
            [0, 0],
            [maxWidth, 0],
            [maxWidth, maxHeight],
            [0, maxHeight]], dtype="float32")

        M = cv2.getPerspectiveTransform(pts, dst)
        warped = cv2.warpPerspective(img, M, (maxWidth, maxHeight))

        return warped

    def __init__(self, max_batch_size=8):
        self.max_batch_size = max_batch_size


class CardCuttingEngine(Engine):
    def process_images(self, paths: List[str], debug=False) -> List[CardCuttingResult]:
        raise NotImplementedError

    def _save_card_img(self, path, result: CardCuttingResult):
        original_img = _cv_load_stub(path)
        dbounds = result.boundingPoly.denorm(original_img.shape)
        new_bounds = self._pad_bounds(dbounds)
        result.boundingPoly = new_bounds.norm(original_img.shape)

        card_img = self.segment_rotate_image(original_img, new_bounds)
        original_dir = os.path.split(path)[0]
        card_path = os.path.join(original_dir, 'card.jpg')
        cv2.imwrite(card_path, card_img)
        card_rel_path = Image.relative_uri(card_path)
        result.image = Image(card_rel_path)

    def _pad_bounds(self, bounding_poly):
        H_PAD_RATIO = 0.05
        W_PAD_RATIO = 0.02
        pts = bounding_poly.array()
        pts = np.asarray(pts, dtype='float32')

        (tl, tr, br, bl) = pts
        blx = bl[0] + H_PAD_RATIO * (bl[0] - tl[0])
        bly = bl[1] + H_PAD_RATIO * (bl[1] - tl[1])
        brx = br[0] + H_PAD_RATIO * (br[0] - tr[0])
        bry = br[1] + H_PAD_RATIO * (br[1] - tr[1])
        
        trx = tr[0] + W_PAD_RATIO * (tr[0] - tl[0])
        try_ = tr[1] + W_PAD_RATIO * (tr[1] - tl[1])
        brx = brx + W_PAD_RATIO * (br[0] - bl[0])
        bry = bry + W_PAD_RATIO * (br[1] - bl[1])

        vx = bounding_poly.vertices[:]
        vx[2] = Vertex(brx, bry)
        vx[3] = Vertex(blx, bly)
        vx[1] = Vertex(trx, try_)
        return BoundingPoly(vx)


    def get_results(self, paths, debug=False):
        results = []
        for i in range(0, len(paths), self.max_batch_size):
            batch = paths[i:i + self.max_batch_size]
            r = self.process_images(batch, debug)

            for rx, px in zip(r, batch):
                self._save_card_img(px, rx)
            r = map(lambda x: CardCuttingResultSchema().dump(x).data, r)
            r = list(r)
            results.extend(r)
        return results


class TextBoxEngine(Engine):
    def process_images(self, path_groups: List[List[str]],  debug=False) -> List[List[TextBoxResult]]:
        raise NotImplementedError

    def _save_box_imgs(self, path, group: List[TextBoxResult], debug=False):
        card_img = _cv_load_stub(path)
        for i, result in enumerate(group):
            dbounds = result.boundingPoly.denorm(card_img.shape)

            if debug:
                RATIO_MAP = {
                    'identityId': (0.2, 0.4),
                    'identityName': (0.07, 0.07),
                    'identityAddress': (0.04, 0.04),
                    'identityBirthplace': (0.04, 0.04),
                    'identityBirthday': (0.2, 0.4)
                }
                width = dbounds.vertices[1].x - dbounds.vertices[0].x
                height = dbounds.vertices[3].y - dbounds.vertices[0].y
                max_height, max_width = card_img.shape[:2]
                exp_ratio = RATIO_MAP[result.field]
                
                dbounds.vertices[0].x -= (width * exp_ratio[0] / 2)
                dbounds.vertices[0].y -= (height * exp_ratio[1] / 2)

                dbounds.vertices[1].x += (width * exp_ratio[0] / 2)
                dbounds.vertices[1].y -= (height * exp_ratio[1] / 2)

                dbounds.vertices[2].x += (width * exp_ratio[0] / 2)
                dbounds.vertices[2].y += (height * exp_ratio[1] / 2)

                dbounds.vertices[3].x -= (width * exp_ratio[0] / 2)
                dbounds.vertices[3].y += (height * exp_ratio[1] / 2)

                # Prevent overflow
                for k in range(4):
                    dbounds.vertices[k].x = min(dbounds.vertices[k].x, max_width)
                    dbounds.vertices[k].x = max(dbounds.vertices[k].x, 0)
                    dbounds.vertices[k].y = min(dbounds.vertices[k].y, max_height)
                    dbounds.vertices[k].y = max(dbounds.vertices[k].y, 0)
                result.boundingPoly = dbounds.norm(card_img.shape)

            box_img = self.segment_rotate_image(card_img, dbounds)
            original_dir = os.path.split(path)[0]
            box_path = os.path.join(original_dir, 'box-%d-%s.jpg' % (i, result.field))
            cv2.imwrite(box_path, box_img)
            box_rel_path = Image.relative_uri(box_path)
            result.image = Image(box_rel_path)

    def get_results(self, paths, debug=False):
        results = []

        for i in range(0, len(paths), self.max_batch_size):
            batch = paths[i:i + self.max_batch_size]
            r = self.process_images(batch, debug)
            for j, group in enumerate(r):
                self._save_box_imgs(batch[j], group, debug=debug)

                group = map(lambda x: TextBoxResultSchema().dump(x).data, group)
                r[j] = list(group)
            results.extend(r)
        return results


class TextDetectionEngine(Engine):
    def process_images(self, paths: List[str], debug=False) -> List[TextDetectionResult]:
        raise NotImplementedError

    def get_results(self, paths, debug=False):
        results = []

        for i in range(0, len(paths), self.max_batch_size):
            batch = paths[i:i + self.max_batch_size]
            r = self.process_images(batch, debug)
            r = list(map(lambda x: TextDetectionResultSchema().dump(x).data, r))
            results.extend(r)
        return results


class PostProcessEngine(Engine):
    def process_strings(self, strings: List[str], debug=False) -> List[PostProcessResult]:
        raise NotImplementedError

    def get_results(self, strings, debug=False):
        results = []
        for i in range(0, len(strings), self.max_batch_size):
            batch = strings[i:i + self.max_batch_size]
            r = self.process_strings(batch, debug)
            r = list(map(lambda x: PostProcessResultSchema().dump(x).data, r))
            results.extend(r)
        return results
