import os
import platform
import sys
from setuptools import setup, find_packages

VERSION = '0.4.2'

requirements = [
    'Flask>=1.0.2',
    'Flask-Cors>=3.0.6',
    'Flask-RESTful>=0.3.6',
    'Flask-LogConfig>=0.4.2',
    'marshmallow>=2.18.1',
    'numpy>=1.16.1',
    'opencv-python>=4.0.0.21',
    'Pillow'
]

setup(
    name="vtlcv",
    version=VERSION,
    description="Shared service library for Viettel CV",
    url="https://lanpn@bitbucket.org/aivn_cv/detection-nodes",
    author="Phan Ngoc Lan",
    author_email="phan.ngoclan58@gmail.com",
    license="",
    packages=find_packages(),
    install_requires=requirements
)
